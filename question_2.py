import sys
import random


def random_walk_area(num_steps=1000):
    x, y = (0, 0)
    min_x, min_y = (0, 0)
    max_x, max_y = (0, 0)
    for _ in range(num_steps):
        if random.random() > .5:
            if random.random() > .5:
                x += 1
            else:
                x -= 1
        else:
            if random.random() > .5:
                y += 1
            else:
                y -= 1
        min_x, min_y = (min(x, min_x), min(y, min_y))
        max_x, max_y = (max(x, max_x), max(y, max_y))
    return (max_x-min_x)*(max_y-min_y)


if __name__ == '__main__':
    print(random_walk_area(int(sys.argv[1])))
