import sys


def print_rectangles(N=25):
    c=u'\u2593'
    b=u'\u2591'
    w=2

    for y in range(N):
        for x in range(N):
            if min(x, N-x-1) > min(y, N-y-1):
                if y % 2 == 0:
                    print(c*w, end='')
                else:
                    print(b*w, end='')
            else:
                if x % 2 == 0:
                    print(c*w, end='')
                else:
                    print(b*w, end='')
        print()
    print()


if __name__ == '__main__':
    print_rectangles(int(sys.argv[1]))
